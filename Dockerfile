ARG KEYCLOAK_VERSION=14.0.0-debian-10-r0
FROM bitnami/keycloak:${KEYCLOAK_VERSION}

ARG KEYCLOAK_DISCORD_VERSION=0.3.1
ADD https://github.com/wadahiro/keycloak-discord/releases/download/v${KEYCLOAK_DISCORD_VERSION}/keycloak-discord-ear-${KEYCLOAK_DISCORD_VERSION}.ear /opt/bitnami/keycloak/standalone/deployments/keycloak-discord-ear-${KEYCLOAK_DISCORD_VERSION}.ear

USER root
RUN chown -R 1001:root /opt/bitnami/keycloak/standalone/deployments/ && \
    chmod +x /opt/bitnami/keycloak/standalone/deployments/keycloak-discord-ear-${KEYCLOAK_DISCORD_VERSION}.ear

USER 1001
